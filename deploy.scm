(use-modules
 (gnu)
 (gnu machine)
 (gnu machine ssh)
 (guix gexp)
 (guix store)
 (guix grafts)
 (guix profiles)
 (gnu packages forth)
 (gnu packages games))

(define n900
  (machine
   (operating-system #f)
   (environment managed-host-environment-type)
   (configuration
    (machine-ssh-configuration
     (host-name "192.168.1.12")
     (system "armhf-linux")
     (user "root")
     (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOtnHUQgnTjfQjtRVrjyFCch7chkEQeTVgI6te5v/gIM")))))

(define %profile
  (profile
   (name "default")
   (content (packages->manifest
	     (list glulxe gforth)))
   (hooks '())))

(parameterize ((%graft? #f))
  (with-store %store
    (run-with-store %store
      (machine-remote-eval
       n900
       #~(let ((dest "/var/guix/profiles/default"))
	   (false-if-exception (delete-file dest))
	   (symlink #$%profile dest))))))
